(function() {
  'use strict';

  angular
    .module('777TapApp')
    .controller('CanTapCtrl', ['$scope', '$timeout', 
      function($scope, $timeout) {
        var timer;

        $scope.counter = 0;
        $scope.countWin = localStorage.getItem('win') || 0;
        $scope.countLost = localStorage.getItem('lost') || 0;

        // show or hide buttons ready and stop
        $scope.toggleBtn = true; 

        $scope.startCount = function() {
          if ($scope.counter) {
            $scope.counter = 0;
          }

          $scope.toggleBtn = false;

          timer = $timeout(function repeatTimer() {
            $scope.counter++;
            timer = $timeout(repeatTimer, 5);

            if ($scope.counter == 1000) {
              $timeout.cancel(timer);
              localStorage.setItem('lost', ++$scope.countLost);
              $scope.counter = 0;
              $scope.toggleBtn = true;
            }
          }, 5);
        };

        $scope.stopCount = function() {
          $timeout.cancel(timer);
          $scope.toggleBtn = true;

          return ($scope.counter != 777) ? 
                  localStorage.setItem('lost', ++$scope.countLost) : 
                  localStorage.setItem('win', ++$scope.countWin);
        };

        $scope.enterKey = function(event) {
          if (event.which != 13) return;

          return ($scope.counter && !$scope.toggleBtn) ? 
                  $scope.stopCount() : $scope.startCount();
        };

        $scope.clear = function() {
          $scope.countLost = $scope.countWin = 0;
          localStorage.setItem('lost', 0);
          localStorage.setItem('win', 0);
        };
        
      }]);
})();
